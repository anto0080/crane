using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Controlls
Fleche
down: x
up:   w
Crochet
down: d
up:   u
Chariot
down: z
up:   s
Chariot
down: o
up:   i
Bras
out:  1
in:   2
Bras2
out:  3
in:   4
Mat
left: o
right:p
*/

public class Mouvement : MonoBehaviour
{
    // Create the camera vars
public Camera cameraA;
public Camera cameraB;
public Camera cameraC;
public Camera cameraD;
    // Start is called before the first frame update
void Start()
{
    // default enable camera A and disable the other Cameras
    cameraA.enabled = true;
    cameraB.enabled = false;
    cameraC.enabled = false;
    cameraD.enabled = false;
}

    // Update is called once per frame
void Update()
{
    //
    // Forward movement (UpArrow moves forward)
    if (Input.GetKey(KeyCode.UpArrow))
    {
        transform.Translate(Vector3.up * 0.003f);
    }

    // Backward movement (DownArrow moves backward)
    if (Input.GetKey(KeyCode.DownArrow))
    {
        transform.Translate(Vector3.down * 0.003f);
    }

    // Rotate left (LeftArrow rotates left)
    if (Input.GetKey(KeyCode.LeftArrow))
    {
        transform.Rotate(Vector3.forward, -0.3f);
    }

    // Rotate right (RightArrow rotates right)
    if (Input.GetKey(KeyCode.RightArrow))
    {
        transform.Rotate(Vector3.forward, 0.3f);
    }
// Change Camera with V key
if (Input.GetKeyDown(KeyCode.V))
    {
        if (cameraA.enabled)
        {
            cameraA.enabled = false;
            cameraB.enabled = true;
        }
        else if (cameraB.enabled)
        {
            cameraC.enabled = true;
            cameraB.enabled = false;
        }
        else if (cameraC.enabled)
        {
            cameraD.enabled = true;
            cameraC.enabled = false;
        }
        else if (cameraD.enabled)
        {
            cameraA.enabled = true;
            cameraD.enabled = false;
        }
    }
}
}
